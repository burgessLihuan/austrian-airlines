import '../../modules/helper-modules/base';
import './style.scss';
var bottoms =$("[data-bottoms='content-bottoms']");
var item_tips =$("[data-tipsjs]");
// window.radio_click = false;
if(!$("#radio").is(':checked')&& radio_click){
    $("#radio").click();
};
$("[data-click='going']").on('click',function () {
    if(radio_click){
        window.showcrossscreen = false;
        var state_arry={};
        state_arry.click = radio_click;
        window.location.href = $("[data-click='going']").data('url')+ '?' + $.param(state_arry);
    }else {
        bottoms.addClass("content-bottoms-err");
    }
});
$("#radio").on('click',function(){
    if(!radio_click){
        radio_click =true;
        bottoms.addClass("opacity-all");
        bottoms.removeClass("content-bottoms-err");
    }else {
        radio_click =false;
        bottoms.removeClass("opacity-all");
        bottoms.removeClass("content-bottoms-err");
    }
});

$("[data-clickjs]").on('click',function (e) {
    var tip_name = $(e.currentTarget).data("clickjs");
    $.each(item_tips,function () {
        if($(this).data("tipsjs") ==tip_name){
            $(this).show();
        }
    })
});
$("[data-click='tips-lose']").on('click',function () {
    item_tips.hide();
});