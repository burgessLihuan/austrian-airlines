import '../../modules/helper-modules/base';
import './style.scss';
var bottomOffsetTop = $('.form-content').offset().top;
var GiveLoading = $("[data-item='give-loading']");
$("[data-click='bottom']").on('click',function () {
    $("[data-from]").each(function () {
        if($(this).data("from") =="name"){
            var nameVal = $.trim($(this).val());
            if(nameVal!=""){
                $(this).removeClass("input-error");
            }else {
                $(this).addClass("input-error");
            }
        }
        else if($(this).data("from") =="phone"){
            var nameVal = $.trim($(this).val());
            var regName = /^1[34578]\d{9}$/;
            if(nameVal!="" && regName.test(nameVal)){
                $(this).removeClass("input-error");
            }else {
                $(this).addClass("input-error");
            }
        }
    });
    if($(".form-list").find(".input-error").length == 0){
        var name_ajax = $(".form-list").find("[data-from='name']").val();
        var phone_ajax = $(".form-list").find("[data-from='phone']").val();
        GiveLoading.addClass('loading-on');
        $.ajax({
            type: 'POST',
            url: window.formUrl + '/score/post',
            data:{name: name_ajax,phone: phone_ajax},
            success:function (flag) {
                GiveLoading.removeClass('loading-on');
                GiveLoading.addClass('weixin-on');
            },
            error:function (flag) {
                console.log("error");
            },
        });
    }
});
$("[data-from]").on("click",function (e) {
    $(e.target).removeClass("input-error");
    $(e.target).val('');
});
$("[data-click='form-icon']").on("click",function () {
    $('.form-page').animate({scrollTop:bottomOffsetTop},800)
});