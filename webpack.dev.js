const webpack = require('webpack');
const Merge = require('webpack-merge');

const CommonConfig = require('./webpack.common.js');


module.exports = function (env, compileEntries) {
  return Merge(CommonConfig(env, compileEntries), {
    devtool: false,

    plugins: [
      // new webpack.LoaderOptionsPlugin({
      //   minimize: true,
      //   debug: false,
      // }),
      //
      // new webpack.DefinePlugin({
      //   'process.env': {
      //     NODE_ENV: JSON.stringify('production'),
      //   },
      // }),

      // new webpack.optimize.UglifyJsPlugin({
      //   beautify: false,
      //   mangle: {
      //     keep_fnames: true,
      //   },
      //   compress: {
      //     drop_console: true,
      //   },
      //   comments: false,
      // }),
    ],
  });
};

